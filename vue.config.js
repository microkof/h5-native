const { defineConfig } = require('@vue/cli-service');
module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: process.env.NODE_ENV === 'production' ? '/h5-native' : '/',
  pages: {
    index: {
      entry: 'src/index/main.js',
      template: 'public/index.html',
      filename: 'index.html',
      title: 'H5仿原生技术演示',
    },
    'toutiao-transition': {
      entry: 'src/toutiao-transition/main.js',
      template: 'public/toutiao-transition.html',
      filename: 'toutiao-transition.html',
      title: '仿今日头条翻页动画及保持滚动位置',
    },
    'weixin-transition': {
      entry: 'src/weixin-transition/main.js',
      template: 'public/weixin-transition.html',
      filename: 'weixin-transition.html',
      title: '仿微信翻页动画及保持滚动位置',
    },
    'close-popup-before-back': {
      entry: 'src/close-popup-before-back/main.js',
      template: 'public/close-popup-before-back.html',
      filename: 'close-popup-before-back.html',
      title: '首次点击回退关闭弹层，再次点击回退真正回退',
    },
    // 'loading-icon': {
    //   entry: 'src/loading-icon/main.js',
    //   template: 'public/loading-icon.html',
    //   filename: 'loading-icon.html',
    //   title: '原生 APP 中菊花图的用法',
    // }
  },
});
