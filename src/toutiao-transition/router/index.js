import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/toutiao-transition/views/Home.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title: '首页',
      isTabBar: true,
    },
  },
  {
    path: '/article-list',
    name: 'ArticleList',
    component: () => import('@/toutiao-transition/views/ArticleList.vue'),
    meta: {
      title: '文章列表',
    },
  },
  {
    path: '/article',
    name: 'Article',
    component: () => import('@/toutiao-transition/views/Article.vue'),
    meta: {
      title: '文章',
    },
  },

  {
    path: '/video-home',
    name: 'VideoHome',
    component: () => import('@/toutiao-transition/views/VideoHome.vue'),
    meta: {
      title: '视频',
      isTabBar: true,
    },
  },
  {
    path: '/video',
    name: 'Video',
    component: () => import('@/toutiao-transition/views/Video.vue'),
    meta: {
      title: '视频',
    },
  },

  {
    path: '/me',
    name: 'Me',
    component: () => import('@/toutiao-transition/views/Me.vue'),
    meta: {
      title: '个人中心',
      isTabBar: true,
    },
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL + 'toutiao-transition.html',
  routes,
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title + ' - 仿今日头条翻页动画';
  next();
});

export default router;
