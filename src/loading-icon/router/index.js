import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/loading-icon/views/Home.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title: '首页',
      isTabBar: true,
    },
  },
  {
    path: '/full-load-article-list',
    name: 'FullLoadArticleList',
    component: () => import('@/loading-icon/views/FullLoadArticleList.vue'),
    meta: {
      title: '完整加载',
    },
  },
  {
    path: '/me',
    name: 'Me',
    component: () => import('@/loading-icon/views/Me.vue'),
    meta: {
      title: '个人中心',
      isTabBar: true,
    },
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL + 'loading-icon.html',
  routes,
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title + ' - 原生 APP 中菊花图的用法';
  next();
});

export default router;
