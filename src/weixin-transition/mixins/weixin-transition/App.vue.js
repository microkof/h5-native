export default {
  data() {
    return {
      transitionName: '',
    };
  },
  watch: {
    $route(to, from) {
      // 先判断“浏览器直接着陆下级页面”还是“由首页逐步进入下级页面”：
      // 如果 from.name === null 表示直接着陆下级页面，此时不应发生动画，也无需考虑滚动位置，所以直接 return 短路即可。
      if (from.name === null) {
        // 直接访问下级页面则设homeIsLandingPage为false，这样导航条后退按钮就不再执行go(-1)，而是push到首页
        this.$store.commit('SET_HOME_IS_LANDING_PAGE', false);
        return;
      }

      // 如果 from.name !== null 表示从首页逐步进入下级页面，此时又分为 2 种情况：
      // 1. 如果 from 和 to 页面都是 Tabbar 级别的页面，就本演示来讲，也就是“首页”、“视频”、“我的”互相切换，这时不应发生动画。应 return。
      // 2. 否则，要发生动画，才是我们唯一需要考虑的。

      /**
       * ***************************** 确定动画 ******************************
       */

      // 在 state 存储 historyRoutes 作为路由历史，它的初始内容见 /store/index.js
      // 现在我们基于 historyRoutes（主要是基于其中的编号）确定路由方向是前进还是回退、继而确定动画方向

      // 判断 from 是否在历史路由中
      let fromInHistory = this.$store.state.historyRoutes.find((p) => p.fullPath === from.fullPath);
      if (!fromInHistory) {
        // 因为 1 是所有 Tabbar 页的编号，所以下级页面的编号从 2 开始
        fromInHistory = { fullPath: from.fullPath, index: 2 };
        this.$store.commit('SET_HISTORY_ROUTES', fromInHistory);
      }

      // 判断 to 是否在历史路由中
      let toInHistory = this.$store.state.historyRoutes.find((p) => p.fullPath === to.fullPath);
      // to 未被访问过，是新页面，意味着入场动画
      if (!toInHistory) {
        // to 的 index 等于 from 的 index 加 1
        toInHistory = { fullPath: to.fullPath, index: fromInHistory.index + 1 };
        this.$store.commit('SET_HISTORY_ROUTES', toInHistory);
        this.transitionName = 'slide-in';
        // Tabbar 页面互相切换，不发生动画，这里 'no-slide' 作为一个标记，在下方“确定滚动条位置”的代码中有用到
      } else if (from.meta.isTabBar && to.meta.isTabBar) {
        this.transitionName = 'no-slide';
      }
      // to 被访问过，且现在是回退操作，意味着离场动画
      else if (fromInHistory.index > toInHistory.index) {
        this.transitionName = 'slide-out';
      }
      // to 被访问过，且是前进操作，意味着入场动画
      else {
        this.transitionName = 'slide-in';
      }

      /**
       * ***************************** 确定滚动条位置 ******************************
       */

      // 旧页面和新页面的滚动位置不同，翻页时如何保证两个页面的滚动条统一：
      // 首先，滚动条以新页面的滚动位置为准，也就是说，让滚动条瞬移到新页面的滚动位置。
      // 因此，原页面只能迁就新页面的滚动位置，这时原页面的滚动位置一定是错的，所以需要同时再给原页面设 margin-top，用来抵消错误。
      // 最终，我们从视觉上就会发现：原、新页面的滚动位置都没有变。

      // 给 from 保存 ScrollY，值是当前滚动条位置
      this.$store.commit('SET_HISTORY_ROUTES', {
        fullPath: from.fullPath,
        scrollY: document.documentElement.scrollTop,
      });

      // 当发生了回退时（回退必须保证新页面的滚动条位置），对应 this.transitionName === 'slide-out'
      // 或发生了需保持滚动条位置的前进时，对应 ((this.transitionName === 'slide-in' || this.transitionName === 'no-slide') && to.params.keepScrollY && toInHistory.scrollY)
      if (
        this.transitionName === 'slide-out' ||
        ((this.transitionName === 'slide-in' || this.transitionName === 'no-slide') &&
          to.params.keepScrollY &&
          toInHistory.scrollY)
      ) {
        // 注：from 页面永远是 #app 的第一个子 div
        //       to 页面永远是 #app 的第二个子 div
        document.querySelector('#app>div:nth-child(1)').style.marginTop =
          toInHistory.scrollY - document.documentElement.scrollTop + 'px';
        document.documentElement.scrollTop = toInHistory.scrollY;
      }
      // else 的情况其实是：当发生不需保持滚动条的前进时
      else {
        document.querySelector('#app>div').style.marginTop = 0 - document.documentElement.scrollTop + 'px';
        document.documentElement.scrollTop = 0;
      }
    },
  },
};
