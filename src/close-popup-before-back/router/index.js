import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/close-popup-before-back/views/Home.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title: '首页',
      isTabBar: true,
    },
  },
  {
    path: '/article-list',
    name: 'ArticleList',
    component: () => import('@/close-popup-before-back/views/ArticleList.vue'),
    meta: {
      title: '文章列表',
    },
  },
  {
    path: '/me',
    name: 'Me',
    component: () => import('@/close-popup-before-back/views/Me.vue'),
    meta: {
      title: '个人中心',
      isTabBar: true,
    },
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL + 'close-popup-before-back.html',
  routes,
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title + ' - 首次点击回退关闭弹层，再次点击回退真正回退';
  next();
});

export default router;
