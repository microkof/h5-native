import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    homeIsLandingPage: true,
    historyRoutes: [
      {
        fullPath: '/',
        index: 1,
      },
      {
        fullPath: '/video-home',
        index: 1,
      },
      {
        fullPath: '/me',
        index: 1,
      },
    ],
  },
  mutations: {
    SET_HOME_IS_LANDING_PAGE(state, bool) {
      state.homeIsLandingPage = bool;
    },
    // obj 的格式可能为 { fullPath, index }，也可能为 { fullPath, scrollY }
    SET_HISTORY_ROUTES(state, obj) {
      const page = state.historyRoutes.find((p) => p.fullPath === obj.fullPath);
      // 未访问过
      if (!page) {
        let item = { fullPath: obj.fullPath };
        if (obj.hasOwnProperty('index')) {
          item.index = obj.index;
        }
        if (obj.hasOwnProperty('scrollY')) {
          item.scrollY = obj.scrollY;
        }
        state.historyRoutes.push(item);
        // 访问过
      } else {
        if (obj.hasOwnProperty('index')) {
          page.index = obj.index;
        }
        if (obj.hasOwnProperty('scrollY')) {
          page.scrollY = obj.scrollY;
        }
      }
    },
  },
});
